var FH = {};

FH.addEvent = function (elm,listener,fn){
	try{
		elm.addEventListener(listener,fn,false);
	}catch(e){
		elm.attachEvent("on"+listener,fn);
	}
};

FH.first = true;

FH.fitHeight = function(options){

	function getClass(classname){
		var allElm = document.body.getElementsByTagName("*");
		var allElmLength = allElm.length;
		var collect = [];
		for (var i=0; i<allElmLength; i++){
			var gotName = allElm[i].className.split(" ");
			for(var j=0; j<gotName.length; j++){
				if(gotName[j] === classname){
				
					if(options.settype === 'type01'){
						collect.push(allElm[i]);
					}else if(options.settype === 'type02'){
			
						var childs = allElm[i].childNodes;
					
						for(var k=0; k<childs.length; k++){
						
							if(childs[k].nodeType === 1){
								collect.push(childs[k]);
							}
						}

					}
				
				}
			}
		}
		return collect;
	}
	
	function getHeight(array){
		var collect = [];
		for(var i=0 ; i < array.length ; i++){
		
			array[i].style.height = '';
		
			var gotHeight = array[i].offsetHeight;
			
			var myStyle = array[i].currentStyle || document.defaultView.getComputedStyle(array[i],"");
			if(myStyle.paddingTop){var padTop = Number(myStyle.paddingTop.replace("px",""));}
			if(myStyle.paddingBottom){var padBtm = Number(myStyle.paddingBottom.replace("px",""));}
			if(myStyle.borderTopWidth){
				if(myStyle.borderTopWidth.match(/[0-9]/) !== null){
					bdTop = Number(myStyle.borderTopWidth.replace("px",""));
				}else{
					bdTop = 0;
				}
			}
			if(myStyle.borderBottomWidth){
				if(myStyle.borderBottomWidth.match(/[0-9]/) !== null){
					bdBtm = Number(myStyle.borderBottomWidth.replace("px",""));
				}else{
					bdBtm = 0;
				}
			}

			var totalHeight = (gotHeight - padTop - padBtm - bdTop - bdBtm);

			collect.push(totalHeight);
		}
		return collect;
	}

	function setHeight(elm,point){		
		for(var i=0 ; i < elm.length ; i++){
			elm[i].style.height = point + 'px';
			
		}		
	}
	
	function resetHeight(){
		var re_heights = getHeight(elements);
		re_heights.sort(function (a,b){return b-a;});
		setHeight(elements,re_heights[0]);		
	}
	
	function setChecker(){

		if(!document.getElementById('FHChecker')){
			var container = document.createElement("div");
			var text = document.createTextNode("&nbsp;"); 
			
			container.setAttribute('id','FHChecker');
			
			container.appendChild(text);
			
			container.style.position = 'absolute';
			container.style.left = '-9999px';
			
			document.body.appendChild(container);
		}
		
		var checker = document.getElementById('FHChecker');
				
		var def_height = checker.offsetHeight;
		
		var chk_height = setInterval(function(){
			
			if(def_height !== checker.offsetHeight){
				resetHeight();
				def_height = checker.offsetHeight;
			}
					
		},options.speed);
		
		FH.first = false;
		
	}
	
	///////////////////////////////////////////////
	var elements = getClass(options.classname);
	var heights = getHeight(elements);
	
	heights.sort(function (a,b){return b-a;});
	setHeight(elements,heights[0]);
	
	setChecker();
	
	///////////////////////////////////////////////
};

FH.addEvent(window,'load',function(){

	FH.fitHeight({
		classname : 'hl_fitter',
		speed : 1000,
		settype : 'type01'
	});
	
	FH.fitHeight({
		classname : 'hl_parent',
		speed : 1000,
		settype : 'type02'
	});

});